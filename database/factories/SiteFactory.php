<?php

namespace Database\Factories;

use App\Models\Site;
use Illuminate\Database\Eloquent\Factories\Factory;

class SiteFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Site::class;

    /**
     * Define the model's default state.
    *
    * @return array
    */

    public function definition() {
        return [
            'name' => $this->faker->name,
            'description' => $this->faker->text,
            'localisation' => $this->faker->text,
            'image' => $this->faker->image('public/images/sites', 2000, 1333, null, false),
        ];
    }
}
