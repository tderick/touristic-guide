<?php

namespace Database\Factories;

use App\Models\Reservation;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReservationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reservation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date_debut' => $this->faker->date,
            'date_fin' => $this->faker->date,
            'name_reservant' => $this->faker->name,
            'country_reservant' => $this->faker->country,
            'ville_reservant' => $this->faker->state,
            'tel_reservant' => $this->faker->phoneNumber,
            'email_reservant' => $this->faker->email
        ];
    }
}
