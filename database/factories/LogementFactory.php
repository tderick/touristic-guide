<?php

namespace Database\Factories;

use App\Models\Logement;
use Illuminate\Database\Eloquent\Factories\Factory;

class LogementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Logement::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->text,
            'localisation' =>$this->faker->text,
            'contact' => $this->faker->phoneNumber,
            'description' => $this->faker->text,
            'image' => $this->faker->image('public/images/sites', 2000, 1333, null, false),
        ];
    }
}
