<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Command;
use App\Models\Image;
use App\Models\Product;
use App\Models\Command_product;
use App\Models\Logement_site;
use App\Models\Reservation_site;
use App\Models\Logement;
use App\Models\Reservation;
use Illuminate\Database\Seeder;
use App\Models\Site;

class DatabaseSeeder extends Seeder {
    /**
    * Seed the application's database.
    *
    * @return void
    */

    public function run() {
        User::factory( 10 )->create();
        Command::factory( 10 )->create();
        Logement::factory(10)->create();
        Product::factory(10)->create();
        Site::factory(10)->create();
        Reservation::factory( 10 )->create();

        //Get all the roles attaching up to 3 random roles to each user
        $sites = Site::all();

        // Populate the pivot table
        Logement::all()->each(function ($logements) use ($sites) { 
        $logements->sites()->attach(
        $sites->random(rand(3, 3))->pluck('id')->toArray()
        ); 
    });

       //Get all the roles attaching up to 3 random roles to each user
       $commands = Command::all();

       // Populate the pivot table
       Product::all()->each(function ($products) use ($commands) { 
       $products->commands()->attach(
       $commands->random(rand(3, 3))->pluck('id')->toArray()
       ); 
   });

      //Get all the roles attaching up to 3 random roles to each user
      $reservations = Reservation::all();

      // Populate the pivot table
      Site::all()->each(function ($sites) use ($reservations) { 
      $sites->reservations()->attach(
      $reservations->random(rand(3, 3))->pluck('id')->toArray()
      ); 
  });

    }
}
