<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\LogementController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\ReservationController;
use App\Models\Logement;
use App\Models\Product;
use App\Models\Site;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// });

Route::get("/", "App\Http\Controllers\HomeController@index")->name('home');
Route::get("/sites","App\Http\Controllers\SiteController@index")->name('site.index');
Route::get("/sites/{id}","App\Http\Controllers\SiteController@show");
Route::get("/boutique","App\Http\Controllers\ProduitController@index")->name('boutique.index');
Route::get("/logements","App\Http\Controllers\LogementController@index")->name('logement.index');
Route::get("/reservation/{siteId}","App\Http\Controllers\ReservationController@index");
// Route::get("/login","App\Http\Controllers\SecurityController@login")->name('login');


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    $siteCount = Site::all()->count();
    $logementCount = Logement::all()->count();
    $productCount = Product::all()->count();
    $usersCount = User::all()->count();

    $generalInformations = ["siteCount"=>$siteCount,
                            "logementCount"=>$logementCount,
                            "productCount"=>$productCount,
                            "usersCount" => $usersCount
    ];
    return Inertia::render('DashboardSummary',[
        'generalInformations' => $generalInformations
    ]);
})->name('dashboard');

Route::group(['auth:sanctum','verified'], function(){
    Route::get("/addSites",[SiteController::class,'add'])->name('site.add');
    Route::post("/saveSite","App\Http\Controllers\SiteController@save")->name('site.save');
    Route::get("/boutique/addProduit","App\Http\Controllers\ProduitController@add")->name('boutique.addProduit');
    Route::post("/produit",[ProductController::class,'store']);
    Route::get("/logements/add","App\Http\Controllers\LogementController@add")->name('logement.add');
    Route::get("/reservations","App\Http\Controllers\LogementController@index")->name('reservation.index');
    Route::post("/site",[SiteController::class,'store']);
    Route::post("/logements",[LogementController::class,'store']);
    Route::get("/reservations","App\Http\Controllers\ReservationController@reserver")->name('reservation.index');
    Route::get("/reservations/show","App\Http\Controllers\ReservationController@show")->name('reservation.show');
    Route::post("/reservation",[ReservationController::class,'store']);
});
