<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Models\Site;

class HomeController extends Controller {
    public function index() {
        $sites = Site::all()->take(6);
         return Inertia::render( 'Home', [
            'sites' => $sites
        ] );
    }
}
