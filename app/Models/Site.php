<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    use HasFactory;

    public function logements(){
        return $this->belongsToMany(Logement::class);
    }

    public function reservations(){
        return $this->belongsToMany(Reservation::class);
    }



}
